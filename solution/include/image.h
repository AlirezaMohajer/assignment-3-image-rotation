#ifndef LAB3_IMAGE_H
#define LAB3_IMAGE_H

#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image create_image(uint64_t width, uint64_t height);

struct image rotate_90(const struct image source_image);

struct image rotate_180(const struct image source_image);

struct image rotate_270(const struct image source_image);

void clear_image(struct image *img);

struct image clone_image(const struct image* source_image);

#endif //LAB3_IMAGE_H
