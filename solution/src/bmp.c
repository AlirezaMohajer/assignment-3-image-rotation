#include "status.h"
#include "image.h"
#include <stdlib.h>

#pragma pack(push, 1)

struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

#pragma pack(pop)
const int IMAGE_SIGNATURE = 0x4D42;

enum read_status from_bmp(FILE *input, struct image *image_data) {

    struct bmp_header image_header;
    fread(&image_header, sizeof(struct bmp_header), 1, input);

    if (image_header.bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }

    image_data->width = image_header.biWidth;
    image_data->height = image_header.biHeight;
    size_t size_image = image_data->width * image_data->height * sizeof(struct pixel);

    image_data->data = (struct pixel *) malloc(size_image);

    fseek(input, image_header.bOffBits, SEEK_SET);

    size_t padding = (4 - (image_data->width * sizeof(struct pixel)) % 4) % 4;

    size_t row = 0;
    while (row < image_data->height) {
        size_t col = 0;
        while (col < image_data->width) {
            fread(&(image_data->data[row * image_data->width + col]), sizeof(struct pixel), 1 ,input);
            col++;
        }
        fseek(input, (long) padding, SEEK_CUR);
        row++;
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *output, const struct image *image_data) {
    struct bmp_header image_header = {
            .bfType = IMAGE_SIGNATURE,
            .bfileSize = sizeof(struct bmp_header) + image_data->width * image_data->height * sizeof(struct pixel),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = image_data->width,
            .biHeight = image_data->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = image_data->width * image_data->height * sizeof(struct pixel),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };

    fwrite(&image_header, sizeof(struct bmp_header), 1, output);

    size_t padding = (4 - (image_data->width * sizeof(struct pixel)) % 4) % 4;


    size_t row = 0;
    while (row < image_data->height) {
        size_t col = 0;
        while (col < image_data->width) {
            if(fwrite(&(image_data->data[row * image_data->width + col]), sizeof(struct pixel), 1, output) != 1){
                return WRITE_CANNOT_WRITING;
            }
            col++;
        }

        uint8_t zero_padding = 0;
        size_t padding_counter = 0;
        while (padding_counter < padding) {
            fwrite(&zero_padding, sizeof(uint8_t), 1, output);
            padding_counter++;
        }
        row++;
    }
    return WRITE_OK;
}
