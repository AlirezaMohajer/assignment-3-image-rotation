#include "image.h"
#include "status.h"
#include <stdlib.h>

struct image create_image(uint64_t width, uint64_t height) {
    struct image img;

    img.height = height;
    img.width = width;
    img.data = (struct pixel *)malloc(width * height * sizeof(struct pixel));

    return img;
}

void clear_image(struct image *img) {
    free(img->data);
    img->width = 0;
    img->height = 0;
    img->data = NULL;
}


struct image clone_image(const struct image* source_image){
    struct image copy_image;
    copy_image.width = source_image->width;
    copy_image.height = source_image->height;

    copy_image.data = (struct pixel*)malloc(source_image->width * source_image->height * sizeof(struct pixel));
    if (!copy_image.data) {
        fprintf(stderr, "Error allocating memory for duplicate image\n");
        exit(EXIT_FAILURE);
    }

    uint64_t i = 0;
    while (i < source_image->width * source_image->height) {
        copy_image.data[i] = source_image->data[i];
        i++;
    }

    return copy_image;
}


struct image rotate_90(struct image const source_image) {
    struct image rotated_image = create_image(source_image.height, source_image.width);
    if (rotated_image.data == NULL) {
        return rotated_image;
    }

    uint64_t y = 0;
    while (y < source_image.height) {
        uint64_t x = 0;
        while (x < source_image.width) {
            rotated_image.data[y + (source_image.width - x - 1) * rotated_image.width] = source_image.data[x + y * source_image.width];
            x++;
        }
        y++;
    }

    return rotated_image;
}

struct image rotate_180(struct image const source_image) {
    struct image rotated_image = create_image(source_image.width, source_image.height);
    if (rotated_image.data == NULL) {
        return rotated_image;
    }
    uint64_t y = 0;
    while (y < source_image.height) {
        uint64_t x = 0;
        while (x < source_image.width) {
            rotated_image.data[(rotated_image.width - x - 1) + (rotated_image.height - y - 1) * rotated_image.width] = source_image.data[x + y * source_image.width];
            x++;
        }
        y++;
    }
    return rotated_image;
}

struct image rotate_270(struct image const source_image) {
    struct image rotated_image = create_image(source_image.height, source_image.width);
    if (rotated_image.data == NULL) {
        return rotated_image;
    }
    uint64_t y = 0;
    while (y < source_image.height) {
        uint64_t x = 0;
        while (x < source_image.width) {
            rotated_image.data[(rotated_image.width - y - 1) + x * rotated_image.width] = source_image.data[x + y * source_image.width];
            x++;
        }
        y++;
    }

    return rotated_image;
}
