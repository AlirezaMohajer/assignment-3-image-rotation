#include "status.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>


int check_argument_count(int arg_count, char *program_name) {
    if (arg_count != 4) {
        fprintf(stderr, "Usage: %s <source-image> <transformed-image> <angle>\n", program_name);
        return 0;
    }
    return 1;
}

FILE* open_input_file(char *filename) {
    FILE *input_file = fopen(filename, "rb");
    if (!input_file) {
        perror("Failed to open the source image");
        return NULL;
    }
    return input_file;
}


int main(int argc, char *argv[4]) {

    if (!check_argument_count(argc, argv[0])) {
        return EXIT_FAILURE;
    }


    FILE *input_file = open_input_file(argv[1]);
    if (!input_file) {
        return EXIT_FAILURE;
    }

    struct image source_image;
    enum read_status _read_status = from_bmp(input_file, &source_image);
    fclose(input_file);

    if (_read_status != READ_OK) {
        fprintf(stderr, "Failed to read the source image: %d\n", _read_status);
        return EXIT_FAILURE;
    }

    //convert a string to an integer value
    int angle = atoi(argv[3]);
    struct image transformed_image;

    if (angle == 0) {
        transformed_image = clone_image(&source_image);
    } else if (angle == 90 || angle == -270) {
        transformed_image = rotate_90(source_image);
    } else if (angle == 180 || angle == -180) {
        transformed_image = rotate_180(source_image);
    } else if (angle == 270 || angle == -90) {
        transformed_image = rotate_270(source_image);
    } else {
        // Handle the invalid angle
        fprintf(stderr, "WRONG ANGLE\nangle will be: 90, -90, 180, -180, 270, -270\n");
        clear_image(&source_image);
        return EXIT_FAILURE;
    }


    FILE *output_file = fopen(argv[2], "wb");
    if (!output_file) {
        perror("Failed to open the transformed image");
        clear_image(&source_image);
        clear_image(&transformed_image);
        return EXIT_FAILURE;
    }

    enum write_status _write_status = to_bmp(output_file, &transformed_image);
    fclose(output_file);

    clear_image(&source_image);
    clear_image(&transformed_image);

    if (_write_status != WRITE_OK) {
        fprintf(stderr, "Failed to write the transformed image: %d\n", _write_status);
        return EXIT_FAILURE;
    }

    printf("Transformation completed successfully...\n");
    return EXIT_SUCCESS;
}
